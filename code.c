#include <avr/io.h>
#define F_CPU 16000000UL
#include <util/delay.h>

int main(){
  first_part();
  third_part();
  while(1)
    second_part();
  fourth_part();
  
	return(0);
 
}
void first_part (){ // first part
  unsigned char num[4]={0b00000100,0b00001100,0b00011100,0b00111100};
  int j;
  int k;
  for(k=0;k<3;k++)
  {
    for(j=0;j<4;j++)
  {
  PORTB=num[j];
  _delay_ms(500);
  }
    PORTB=0b00000000;
  _delay_ms(1000);
  }
}

  void second_part(){ // 2nd part
    DDRB = 0b00000010;
	if(PINB & 0b00000001)
    {
    PORTB=0b00000000;
     _delay_ms(100);
    }
    else {
        PORTB=0b00000010;
        _delay_ms(100);
      }
}
void third_part (){
  int i;
  int k;
  DDRD = 0xff; // 3rd part
  unsigned char numbers[10]={0xFC,0x60,0xDA,0xF2,0x66,0xB6,0xBE,0xE0,0xFE,0xF6};
  for(k=0;k<2;k++)
  {
  	for(i=0;i<10;i++)
  	{
  	PORTD=numbers[i];
  	_delay_ms(400);
  }
  }
}

void fourth_part(){
  int state = 0;
  int i;
  unsigned char numbers[4]={0b00100000,0b00010000,0b00001000,0b00000100};
  unsigned char reversed[4]={0b00000100,0b00001000,0b00010000,0b00100000};
	while(1)
    {
  if(PINB & 0b00000001)
  {
    if (state == 0)
    state = 1;
    else 
    state = 0;
    _delay_ms(500);
  }
	if(state == 0)
      for(i=0;i<4;i++)
    {
      PORTB = numbers[i];
      _delay_ms(300);
    }
      else
        for(i=0;i<4;i++)
      {
        PORTB = reversed[i];
      	_delay_ms(300);
      }
    
    }
}